Traefik Docker
==============

This role will install traefik in docker compose with ssl cert from cloudflare

Requirements
------------

server with docker

Role Variables
--------------

See in defaults

Example Playbook
----------------

```yaml
- name: Install traefik
  hosts: all
  become: true
  tags: traefik

  vars:
    traefik_zone: "valid.zone"
    traefik_dns_challenge_email: "valid+email@gmail.com"

  roles:
    - role: traefik-docker
```

TODO
----

- [] create network separately
- [] make basic auth password more secure
- [] add ability to deploy additional configs
- [] move more traefik configs to vars

Author Information
------------------

Made by Georgy Shapiro
contact me at <geoshapka@gmail.com>
